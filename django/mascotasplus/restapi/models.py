# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Tcompanion(models.Model):
    requester = models.ForeignKey('Tuser', models.DO_NOTHING)
    walk = models.ForeignKey('Twalk', models.DO_NOTHING)
    message = models.CharField(max_length=500)
    is_accepted = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'tCompanion'


class Tuser(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=100)
    email = models.CharField(unique=True, max_length=200)
    encrypted_password = models.CharField(max_length=100)
    active_session_token = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tUser'


class Twalk(models.Model):
    datetime = models.DateTimeField()
    latitude = models.DecimalField(max_digits=8, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    author = models.ForeignKey(Tuser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'tWalk'
